#!/usr/bin/env bash
set -o errexit
set -o nounset

##  
##
##

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )


TARGET="${DIR}/ext/bwa-0.7.12/"
echo "********************************"
echo "Entering ${TARGET}"
echo "********************************\n"
cd ${TARGET}
make clean;
make;
cd ${DIR} 
echo ""
echo "Internal and external libraries successfully compiled"
echo "You may want to run ./test_simple.sh and ./test_parametric.sh"
echo "to verify that the pipeline is ready to use."


