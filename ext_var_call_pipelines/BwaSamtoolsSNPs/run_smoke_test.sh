#!/usr/bin/env bash
set -o errexit
set -o nounset

REFES=../test_data/reference.fasta
READS=../test_data/reads1.fq


./pipeline.sh  ${REFES} ${READS} ./output.vcf
