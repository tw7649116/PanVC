#!/usr/bin/env bash
EXT=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

BWA_BIN=${EXT}/bwa-0.7.12/bwa
SAMTOOLS_BIN=${EXT}/samtools-0.1.19/samtools
BCFTOOLS_BIN=${EXT}/samtools-0.1.19/bcftools/bcftools
VCFUTILS_BIN=${EXT}/samtools-0.1.19/bcftools/vcfutils.pl

PICARD_JAR=${EXT}/picard-tools-1.125/picard.jar
#PICARD_JAR=${EXT}/picard-tools-1.128/picard.jar  # there was a bug with this particular version and GATK
GATK_JAR=${EXT}/GATK/GenomeAnalysisTK.jar
