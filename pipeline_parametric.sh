#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail


## TODO: change readlink ?
## fail was not clear when the file was not available
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source "${DIR}/utils.sh"
source "${DIR}/config.sh"


SENSIBILITY=5
MAX_RECOMB=0

if [ "$#" -ne 3 ]; then
  echo "Script: '${0}'"
  echo "uses 3 params instead of $#" 
  exit
fi

utils_assert_file_exists ${1}
utils_assert_file_exists ${2}
utils_assert_dir_exists ${3}

PAN_GENOME_REF=`readlink -f ${1}`
READS_FILE=`readlink -f ${2}`
WORKING_FOLDER=`readlink -f ${3}`

SAM_OUTPUT_FOLDER=${WORKING_FOLDER}/sam_files
## We dont create sam folder because this will lead the aligner 
## to assume that the reads were already aligned.
ADHOC_REF_OUTPUT_FOLDER=${WORKING_FOLDER}/adhoc_ref_files
mkdir -p ${ADHOC_REF_OUTPUT_FOLDER}

echo "Pan Genome Ref: ${PAN_GENOME_REF}"
echo "Reads file: ${READS_FILE}"
echo "Sam output: ${SAM_OUTPUT_FOLDER}"
echo "Ad Hoc Ref output: ${ADHOC_REF_OUTPUT_FOLDER}"

# Each of the internal tools that will be used.

INDEX_SH="${PG_INDEX_DIR}/index.sh"
ALIGN_SH="${PG_INDEX_DIR}/align_reads.sh"
SAM_TO_POS_SH="${DIR}/components/sam_to_pos/process.sh"
PATHS_SH="${DIR}/components/heaviest_paths/paths.sh"
NORMALIZE_VCF_SH="${DIR}/components/normalize_vcf/normalize.sh"
utils_assert_file_exists ${INDEX_SH}
utils_assert_file_exists ${ALIGN_SH}
utils_assert_file_exists ${SAM_TO_POS_SH}
utils_assert_file_exists ${PATHS_SH}
utils_assert_file_exists ${NORMALIZE_VCF_SH}



echo "=================================="
echo "Indexing $PAN_GENOME_REF..."
echo "=================================="
# generate the index, in the same location as tha pan genome reference is
#./components/pan_genome_index/index.sh $PAN_GENOME_REF
time ${INDEX_SH} ${PAN_GENOME_REF}


echo "=================================="
echo "Aligning reads from $READS_FILE..."
echo "=================================="
#Align the reads, generating one sam file per individual in the pan reference
${ALIGN_SH} $PAN_GENOME_REF $READS_FILE $SAM_OUTPUT_FOLDER


echo "=================================="
echo "Converting Sam to Pos"
echo "=================================="
${SAM_TO_POS_SH} $PAN_GENOME_REF $SAM_OUTPUT_FOLDER $SENSIBILITY


echo "=================================="
echo "Generating ad hoc reference"
echo "=================================="
${PATHS_SH} $PAN_GENOME_REF $SAM_OUTPUT_FOLDER $MAX_RECOMB $ADHOC_REF_OUTPUT_FOLDER


## UP TO THIS POINT THE PREPROCESSING STAGE IS DONE. 
## NOW WE CALL GATK (or any other Var calll pipeline that was specified)

TMP_VCF_FILE="${ADHOC_REF_OUTPUT_FOLDER}/variants_relative_to_adhoc.vcf"

ADHOC_REFERENCE_PREFIX="${ADHOC_REF_OUTPUT_FOLDER}/adhoc_reference"
ADHOC_REFERENCE_FILE_FASTA="${ADHOC_REFERENCE_PREFIX}.fasta"
${EXT_VCF_TOOL_SH} ${ADHOC_REFERENCE_FILE_FASTA} ${READS_FILE} ${TMP_VCF_FILE}

######################

# Now we "normalize" the vcf, that is, we map it to the original reference instead to the adhoc
# reference.
${NORMALIZE_VCF_SH} ${TMP_VCF_FILE} ${ADHOC_REFERENCE_PREFIX} ${PAN_GENOME_REF}
TMP_NORMALIZED_VCF=${TMP_VCF_FILE}.normalized.vcf
NORMALIZED_VCF=${WORKING_FOLDER}/variations.vcf

cp ${TMP_NORMALIZED_VCF} ${NORMALIZED_VCF}

echo "Pipeline succedded. Variations file: ${NORMALIZED_VCF}"

