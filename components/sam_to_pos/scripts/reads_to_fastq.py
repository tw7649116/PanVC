import sys

def column(file_name):
  f = open(file_name);
  i = 0;
  for line in f:
    i = i+1;
    print "@FROM "+file_name+" line: "+str(i);
    print line.strip();
    print "+";
    line_4 = "2"*len(line.strip());
    print line_4;
  f.close();

n_args = len(sys.argv);
if(n_args != 2):
  print 'Number of arguments:', n_args, 'arguments, is incorrect:'
  sys.exit();
FILE_NAME=sys.argv[1];
column(FILE_NAME);
