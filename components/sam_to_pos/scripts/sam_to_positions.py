#!/usr/bin/env python
import sys
import re
import StringIO
import gzip

### Assumptuions:
### The sam record contains the pattern, we are not covered for the * case, so it will trigger an
### index out of bouds while verifying pattern.

op_finder = re.compile('\d\d*\D')

# SENSIBILITY = NONE == 0 
# Means that no error will be accepted.
# SENSIBILITY = ALL means max_error = infinity.
# Any integer is accepted and is the max error allowed.


# return 1 / 0 depending on if the string was successfully understood or no
def cigar_to_inervals(cigar_string, pattern, start_pos, reference, max_error):
  
  #print >> sys.stderr, 'Reference: ' + str(reference)
  #print >> sys.stderr, 'Reference length: ' + str(len(reference))
  count_errors = 0;
  output = StringIO.StringIO()
  original_cigar = cigar_string
  #print original_cigar
  #print pattern
  #print start_pos
  while (len(cigar_string) != 0):
    #process operations in CIGAR string:
      #print "sp:"+str(start_pos);
      op = op_finder.match(cigar_string);
      op_str = op.group();
      #cigar_string_range = op.span();
      cigar_string=cigar_string[op.end():]
      #print cigar_string;
      op_length = int((op_str[:-1]));
      op_type = (op_str[-1]);
      if(op_type == "M"):
        #validate is match:
        next_pos = start_pos + op_length;
        match_count = 0;
        report_pos = start_pos;
        for i in range(op_length):
          #print >> sys.stderr, 'Inspecting: ' + str(start_pos) + "+"+str(i) +":"+ str(start_pos+i)
          if (reference[start_pos + i] == pattern[i]):
            if (match_count == 0):
              report_pos = start_pos + i;
            match_count += 1;
          else:
            count_errors += 1; 
            if (count_errors > max_error):
              return True
            if (match_count != 0):
              line=(str(report_pos)+" "+str(match_count));
              output.write(line+"\n");
            ##start_pos = start_pos + i;
            match_count = 0;
        if (match_count!= 0):
          line=(str(report_pos)+" "+str(match_count));
          output.write(line+"\n");
        #else:
        #  print >> sys.stderr, original_cigar+'[XXXXXXX]CIGAR is a happy match'; 
        start_pos = start_pos + op_length;
        pattern = pattern[op_length:];
      elif (op_type == "D"):
        # Deletion from the reference:
        # We only advance the start position. dont reduce pattern, dont emit nothing.
        count_errors += op_length 
        if (count_errors > max_error):
          return True
        start_pos = start_pos + op_length;
      elif (op_type == "S"):
        # we dont move start position: 
        # we are processing an insertion in the pattern, we procced to remove this area from the pattern, and that is
        count_errors += op_length 
        if (count_errors > max_error):
          return True
        pattern = pattern[op_length:];
      elif (op_type == "I"):
        # we dont move start position: 
        # we are processing an insertion into  the referecne, we procced to remove this area from the pattern, and that is
        count_errors += op_length 
        if (count_errors > max_error):
          return True
        pattern = pattern[op_length:];
      elif (op_type == "H"):
        # we dont do nothing. The string in pattern was already "HARD CLIPPED".
        count_errors += op_length 
        if (count_errors > max_error):
          return True
      else: 
        print >> sys.stderr, original_cigar+'[XXXXXXX]CIGAR string unkkown.';
        return True;
  if (len(pattern) > 0):
    print "PROOOOOBLEMOOO!. Inconsitency in sam format!";
    sys.exit(1);
    return True;
  print output.getvalue().rstrip(); 
  output.close()
  return False;

# open a 1-seq fasta file
def read_fasta(file_name):
  fp = open(file_name);
  for line in fp:
    line = line.rstrip()
    if line.startswith(">"):
      continue;
    else:
      return line;


def sam_process(file_name, reference, max_error):
  f = gzip.open(file_name);
  not_maped= 0;
  cigar_unknown = 0;
  maped = 0;
  for line in f:
    values = line.split('\t');
    if (len(values) < 9):
      continue;
    if (values[1] == '4'):
      not_maped = not_maped + 1
      continue;
    cigar_string = values[5];
    pattern = values[9];
    start_pos = int(values[3]) - 1;
    #print >> sys.stderr, 'Processing:'
    #print >> sys.stderr, line
    error = cigar_to_inervals(cigar_string, pattern, start_pos, reference, max_error);
    if (error):
      cigar_unknown = cigar_unknown + 1;
    else:
      maped = maped + 1;

  f.close();
  return (maped, cigar_unknown, not_maped);

n_args = len(sys.argv);
if(n_args != 4):
  print 'Number of arguments:', n_args, 'arguments, is incorrect:'
  sys.exit();

#print >> sys.stderr, 'Running:'
#print >> sys.stderr, sys.argv[0] + ' ' + sys.argv[1]+ ' ' + sys.argv[2] + ' ' + sys.argv[3]

SAM_FILE=sys.argv[1];
REFERENCE_FILE_NAME=sys.argv[2];
SENSIBILITY=sys.argv[3];
reference = read_fasta(REFERENCE_FILE_NAME);
#print >> sys.stderr, 'Reference readed. Length:'+str(len(reference));
if (SENSIBILITY == 'NONE'):
  #max_error = True;
  max_error = 0;
  print >> sys.stderr, '[sam_to_positions]: ****** Only exact matches accepted. *******';
elif (SENSIBILITY == 'ALL'):
  #max_error = False;
  max_error = float("inf")
  print >> sys.stderr, '[sam_to_positions]: ****** All matches accepted. *******';
else:
  #max_error = False;
  max_error = int(SENSIBILITY);
  print >> sys.stderr, '[sam_to_positions]: ****** Max size of mismatch/indels is:'+ SENSIBILITY +'*******';


(maped, cigar_unknown, not_maped) =  sam_process(SAM_FILE, reference, max_error);
total = maped + cigar_unknown + not_maped;
print >> sys.stderr, '[sam_to_positions]: *************************';
print >> sys.stderr, '[sam_to_positions]: Reports of mapes reads REPORT(MAPED|CIGAR_UNKNOWN|NOT_MAPED|TOTAL)';
print >> sys.stderr, 'REPORT('+str(maped)+'|'+str(cigar_unknown)+'|'+str(not_maped)+'|'+str(total)+')';
print >> sys.stderr, '[sam_to_positions]: *************************';
