#!/usr/bin/env bash
set -o errexit
set -o nounset

if [ "$#" -ne 9 ]; then
  echo "Illegal number of parameters. I should be called from the pipeline script, with 9 params instead of $# " 
  exit 33
else
  echo "OK Params"
fi
OUTPUT_FOLDER=${1}
PARAM_READS_FOLDER=${2}
SRC_READS_ARE_PLAIN=${3}
PARAM_SENSIBILITY=${4}
MY_SEQS=${5}
FIRST_REF=${6}
LAST_REF=${7}
FIRST_TEST=${8}
LAST_TEST=${9}

LOCAL_READS_FOLDER=${OUTPUT_FOLDER}/TMP_READS/
LOCAL_SAM_FOLDER=${OUTPUT_FOLDER}/SAM/

# TODO: check that the DEPENDENCIES exist:
# sam_to_positions.py
# reads_to_fastq.py for the exact reads only
# gap_positions.py

#Defines BWA_BIN
source ../../ext/set_env.sh

if [ ! -f "${BWA_BIN}" ]; then
  echo "File '${BWA_BIN}'not found!. File: '${0}'"
  exit 33
fi


# Transform plain reads to fq format.
transformPlainReadsToFQ() {
FUNC_READS_FOLDER=$1
for (( CURRENT_ID=$FIRST_TEST; CURRENT_ID<=$LAST_TEST; CURRENT_ID++ ))
do
  PLAIN_READ_GZ="${FUNC_READS_FOLDER}/recombinant.n${CURRENT_ID}.reads.gz"
  if [ ! -f "${PLAIN_READ_GZ}" ]; then
    echo "File '${PLAIN_READ_GZ}'not found!"
    exit 33
  fi
  PLAIN_READ="${LOCAL_READS_FOLDER}/recombinant.n${CURRENT_ID}.reads"
  cp $PLAIN_READ_GZ $LOCAL_READS_FOLDER/
  gunzip $PLAIN_READ  
  READS_FILE="${LOCAL_READS_FOLDER}/wgsim.n${CURRENT_ID}.fq"
  python reads_to_fastq.py  $PLAIN_READ > $READS_FILE
  rm $PLAIN_READ 
done
echo "Reads translated to fq"
}
export -f transformPlainReadsToFQ

findReadsGeneric() { 
  echo "Finding occurrences of strain  $1 in $2"
  STRAIN_ID=$1
  SEQ_FILE=$2
  FUNC_READS_FOLDER=$3
  SAM_FILE="${LOCAL_SAM_FOLDER}/mapReads${STRAIN_ID}Sequence${CURRENT_REFERENCE}.sam.gz"
  AUX_READS_FILE="${FUNC_READS_FOLDER}/wgsim.n${STRAIN_ID}.fq"
  $BWA_BIN mem -t 7 $SEQ_FILE $AUX_READS_FILE | gzip > $SAM_FILE
}
export -f findReadsGeneric 

processSam() { 
  echo "Finding occurrences of strain  $1 in $2"
  STRAIN_ID=$1
  SEQ_FILE=$2
  SENSIBILITY=$3
  SAM_FILE="${LOCAL_SAM_FOLDER}/mapReads${STRAIN_ID}Sequence${CURRENT_REFERENCE}.sam.gz"
  POSITIONS_FILE="${OUTPUT_FOLDER}/mapReads${STRAIN_ID}Sequence${CURRENT_REFERENCE}.pos"
  
  if [ ! -f "${SAM_FILE}" ]; then
    echo "File '${SAM_FILE}'not found!. File: '${0}'"
    exit 33
  fi
  if [ ! -f "${SEQ_FILE}" ]; then
    echo "File '${SEQ_FILE}'not found!. File: '${0}'"
    exit 33
  fi
  
  python sam_to_positions.py $SAM_FILE $SEQ_FILE $SENSIBILITY > $POSITIONS_FILE 
}
export -f processSam 


############################################################# MAIN
if [ ${SRC_READS_ARE_PLAIN} -eq 1 ]; then
  echo "Plain Reads."
  if [ -d $LOCAL_READS_FOLDER ]; then
    echo "Reads were already transfomed"
  else
    echo "We transform themto wgsim"
    mkdir ${LOCAL_READS_FOLDER}
    transformPlainReadsToFQ $PARAM_READS_FOLDER
  fi
  ACTUAL_READS=$LOCAL_READS_FOLDER
else
  echo "WGSIM Reads. We don't do anything"
  ACTUAL_READS=$PARAM_READS_FOLDER
fi

if [ -d $LOCAL_SAM_FOLDER ]; then
  echo "SAMS were already computed. We don't align again"
else
  echo "Generating SAM"
  mkdir ${LOCAL_SAM_FOLDER}
  for (( CURRENT_REFERENCE=$FIRST_REF; CURRENT_REFERENCE<=$LAST_REF; CURRENT_REFERENCE++ ))
  do
    # Make the calls
    SEQUENCE_FILE=$MY_SEQS/recombinant.n$CURRENT_REFERENCE.fa
    for (( READ_ID=$FIRST_TEST; READ_ID<=$LAST_TEST; READ_ID++ ))
    do
      findReadsGeneric $READ_ID $SEQUENCE_FILE $ACTUAL_READS #&
    done
    wait
  done
fi

# Final call is done allways:
for (( CURRENT_REFERENCE=$FIRST_REF; CURRENT_REFERENCE<=$LAST_REF; CURRENT_REFERENCE++ ))
do
  # Make the calls
  SEQUENCE_FILE=$MY_SEQS/recombinant.n$CURRENT_REFERENCE.fa
  for (( READ_ID=$FIRST_TEST; READ_ID<=$LAST_TEST; READ_ID++ ))
  do
    processSam $READ_ID $SEQUENCE_FILE $PARAM_SENSIBILITY #&
  done
  wait
done

