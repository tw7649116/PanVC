#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source ${DIR}/config.sh
source ${DIR}/../../utils.sh

if [ "$#" -ne 1 ]; then
  echo "Illegal number of parameters. I use 1 params instead of $# " 
  exit 33
fi

utils_assert_file_exists ${MSA_2_FASTAS}
utils_assert_file_exists ${CHIC_INDEX_BIN}
utils_assert_file_exists ${GAP_POSITIONS_PY}

PAN_REFERENCE_FILE=$1
utils_assert_file_exists ${PAN_REFERENCE_FILE}


TMP_FOLDER=$(dirname "${PAN_REFERENCE_FILE}")
TMP_BASE=`basename $PAN_REFERENCE_FILE`
TMP_PREFIX=${TMP_BASE%.*}
PAN_REFERENCE_FOLDER=${TMP_FOLDER}/${TMP_PREFIX}.index/

if [ -d "${PAN_REFERENCE_FOLDER}" ]; then
  if [ ${REUSE_INDEX} -eq 1 ]; then
    echo "Folder-Index '${PAN_REFERENCE_FOLDER}' and flag REUSE_INDEX is set.
    we will not index again. File: '${0}'"
    exit 0
  fi
fi
mkdir -p ${PAN_REFERENCE_FOLDER}

N_REFS=$( zcat ${PAN_REFERENCE_FILE}  | tail -n1 | wc -c)
N_REFS=`expr $N_REFS - 1`
echo "Reference contains ${N_REFS} individuals"

## NEW CODE SHOULD GENERATE THE SAME...
zcat ${PAN_REFERENCE_FILE} > ${PAN_REFERENCE_FILE}.plain

# This generates the ${SEQUENCE_FULL} files in PAN_REFERENCE_FOLDER.
cd ${PAN_REFERENCE_FOLDER}
${MSA_2_FASTAS} ${PAN_REFERENCE_FILE}.plain 
cd ${DIR}

SEQUENCE_ALL_FILE=$PAN_REFERENCE_FOLDER/recombinant.all.fa
rm -f ${SEQUENCE_ALL_FILE}
for (( CURRENT_REFERENCE=1; CURRENT_REFERENCE<=${N_REFS}; CURRENT_REFERENCE++ ))
do
  SEQUENCE_FILE=$PAN_REFERENCE_FOLDER/recombinant.n$CURRENT_REFERENCE.fa

  SEQUENCE_FULL=$PAN_REFERENCE_FOLDER/recombinant.n$CURRENT_REFERENCE.full
  utils_assert_file_exists ${SEQUENCE_FULL}
  
  echo ">PG_REF_${CURRENT_REFERENCE}" >> $SEQUENCE_ALL_FILE
  echo ">PG_REF_${CURRENT_REFERENCE}" > $SEQUENCE_FILE  # redudant, just to keep backward compatibility for the next script.
  
  # This was replaced by the call to MSA_2_FASTAS which is more efficient.
  #zcat ${PAN_REFERENCE_FILE} |awk '{printf("%s", substr($0,'${CURRENT_REFERENCE}',1))}' > ${SEQUENCE_FULL}
  
  cat ${SEQUENCE_FULL} | tr -d '-' >> ${SEQUENCE_ALL_FILE}
  cat ${SEQUENCE_FULL} | tr -d '-' >> ${SEQUENCE_FILE}
  echo "" >> ${SEQUENCE_ALL_FILE}  ## the missing endline

  GAP_POS_FILE=$PAN_REFERENCE_FOLDER/recombinant.n${CURRENT_REFERENCE}.gap_positions
  python ${GAP_POSITIONS_PY} ${SEQUENCE_FULL} > ${GAP_POS_FILE}

  ## TODO: redirect bwa output to a log file
done
$CHIC_INDEX_BIN  $SEQUENCE_ALL_FILE ${MAX_QUERY_LEN}

