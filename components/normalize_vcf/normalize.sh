#!/usr/bin/env bash
set -o errexit
set -o nounset

if [ "$#" -ne 3 ]; then
  echo "Script: '${0}':"
  echo "Illegal number of parameters. I use 3 parameters instead of $# " 
  exit 33
else
  echo "OK Params"
fi

TMP_VCF_FILE=${1}
ADHOC_REFERENCE_PREFIX=${2}
PAN_REFERENCE_FILE=${3}

TMP_FOLDER=$(dirname "${PAN_REFERENCE_FILE}")
TMP_BASE=`basename $PAN_REFERENCE_FILE`
TMP_PREFIX=${TMP_BASE%.*}
PAN_REFERENCE_FOLDER=${TMP_FOLDER}/${TMP_PREFIX}.index/

ADHOC_REFERENCE_FILE_FASTA=${ADHOC_REFERENCE_PREFIX}.fasta
ADHOC_REFERENCE_ALIGNED_TO_REF=${ADHOC_REFERENCE_PREFIX}.aligned_to_ref

if [ ! -f $1 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${1}'. Quitting. "
  exit 33
fi

if [ ! -f ${ADHOC_REFERENCE_FILE_FASTA} ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${ADHOC_REFERENCE_FILE_FASTA}'. Quitting. "
  exit 33
fi

if [ ! -f ${ADHOC_REFERENCE_ALIGNED_TO_REF} ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${ADHOC_REFERENCE_ALIGNED_TO_REF}'. Quitting. "
  exit 33
fi

if [ ! -f $3 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${3}'. Quitting. "
  exit 33
fi


DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )


## APLY VCF TO TMP VARS TO ADHOC REF
ALIGNED_VARS="${TMP_VCF_FILE}.applied"

echo "[normalize.sh] will call command: python ${DIR}/apply_vcf.py $ADHOC_REFERENCE_FILE_FASTA $TMP_VCF_FILE > ${ALIGNED_VARS}"
python ${DIR}/apply_vcf.py $ADHOC_REFERENCE_FILE_FASTA $TMP_VCF_FILE > ${ALIGNED_VARS}

A1=${PAN_REFERENCE_FOLDER}/recombinant.n1.full
A2=${ADHOC_REFERENCE_ALIGNED_TO_REF}

X1="${TMP_VCF_FILE}.applied.seq1"
X2="${TMP_VCF_FILE}.applied.seq2"
head -n1 ${ALIGNED_VARS} | tr -d '\n' > ${X1}
tail -n1 ${ALIGNED_VARS} | tr -d '\n' > ${X2}

${DIR}/validate_equal_sequences.sh ${A2} ${X1}

OUTPUT_PREFIX="${TMP_VCF_FILE}"
echo "${DIR}/projector/projector ${A1} ${A2} ${X1} ${X2} ${OUTPUT_PREFIX}"
${DIR}/projector/projector ${A1} ${A2} ${X1} ${X2} ${OUTPUT_PREFIX}

MSA=${OUTPUT_PREFIX}.msa
MSA_TO_VCF=${DIR}/jvarkit/dist-1.128/biostar94573
NORMALIZED_VCF=${TMP_VCF_FILE}.normalized.vcf

${MSA_TO_VCF} ${MSA} > ${NORMALIZED_VCF}
