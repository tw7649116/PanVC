/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <sys/times.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <vector>
#include "./utils.h"
#include "./debug.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

using std::vector;
using std::pair;

bool global_fail = false;
void fail();
void success();
void summary();

void fail() {
  printf(ANSI_COLOR_RED "\tTest Failed\n\n" ANSI_COLOR_RESET);
  global_fail = true;
}
void success() {
  printf(ANSI_COLOR_BLUE "\tSuccess\n\n" ANSI_COLOR_RESET);
}

void summary() {
  if (!global_fail) {
    printf(ANSI_COLOR_GREEN "\n\tALL UNIT TEST PASSED\n" ANSI_COLOR_RESET);
  } else {
    printf(ANSI_COLOR_RED "\n\tSOME UNIT TESTS FAILED\n" ANSI_COLOR_RESET);
  }
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////


void testProjector1();
void testProjector1() {
  printf("Testing Projector 1:\n");
  size_t A_len = 4;
  size_t X_len = 4;
  uchar A[4] = {'a', 'a', 'c', 't'};  // NOLINT
  uchar B[4] = {'a', 'a', 'c', 't'};  // NOLINT
  uchar X[4] = {'a', 'a', 'c', 't'};  // NOLINT
  uchar Y[4] = {'a', 'a', 'c', 't'};  // NOLINT
  
  uchar nA[4] = {'a', 'a', 'c', 't'};  // NOLINT
  uchar nY[4] = {'a', 'a', 'c', 't'};  // NOLINT
  size_t n_len = 4;
  
  uchar * newA;
  uchar * newY;
  size_t new_len;

  Utils::ProjectAlignment(A, B, A_len, X, Y, X_len, &newA, &newY, &new_len);
  
  if (n_len != new_len) { 
    fail();
    return;
  }
  
  if (! Debug::EqualArrays<uchar>(nA, newA, n_len)) {
    fail();
    return;
  }
  if (! Debug::EqualArrays<uchar>(nY, newY, n_len)) {
    fail();
    return;
  }
  delete [] newA;
  delete [] newY;
  success();
}

void testProjector2();
void testProjector2() {
  printf("Testing Projector 2:\n");
  size_t A_len = 6;
  uchar A[6] = {'a', 'c', 't', 'a', 'c', 't'};  // NOLINT
  uchar B[6] = {'a', '-', '-', 'a', 'c', 't'};  // NOLINT
  
  size_t X_len = 4;
  uchar X[4] = {'a', 'a', 'c', 't'};  // NOLINT
  uchar Y[4] = {'a', 'a', 'c', 't'};  // NOLINT
  
  size_t n_len = 6;
  uchar nA[6] = {'a', 'c', 't', 'a', 'c', 't'};  // NOLINT
  uchar nY[6] = {'a', '-', '-', 'a', 'c', 't'};  // NOLINT
  
  uchar * newA;
  uchar * newY;
  size_t new_len;

  Utils::ProjectAlignment(A, B, A_len, X, Y, X_len, &newA, &newY, &new_len);
  
  if (n_len != new_len) { 
    fail();
    return;
  }
  
  if (! Debug::EqualArrays<uchar>(nA, newA, n_len)) {
    fail();
    return;
  }
  if (! Debug::EqualArrays<uchar>(nY, newY, n_len)) {
    fail();
    return;
  }
  delete [] newA;
  delete [] newY;
  success();
}

void testProjector3();
void testProjector3() {
  printf("Testing Projector 3:\n");
  size_t A_len = 6;
  uchar A[6] = {'a', 'c', 't', 'a', 'c', 't'};  // NOLINT
  uchar B[6] = {'a', '-', '-', 'a', 'c', 't'};  // NOLINT
  
  size_t X_len = 5;
  uchar X[5] = {'a', 'a', 'c', '-', 't'};  // NOLINT
  uchar Y[5] = {'a', 'a', 'c', 'c', 't'};  // NOLINT
  
  size_t n_len = 7;
  uchar nA[7] = {'a', 'c', 't', 'a', 'c', '-', 't'};  // NOLINT
  uchar nY[7] = {'a', '-', '-', 'a', 'c', 'c', 't'};  // NOLINT
  
  uchar * newA;
  uchar * newY;
  size_t new_len;

  Utils::ProjectAlignment(A, B, A_len, X, Y, X_len, &newA, &newY, &new_len);
  
  if (n_len != new_len) { 
    fail();
    return;
  }
  
  if (! Debug::EqualArrays<uchar>(nA, newA, n_len)) {
    fail();
    return;
  }
  if (! Debug::EqualArrays<uchar>(nY, newY, n_len)) {
    fail();
    return;
  }
  delete [] newA;
  delete [] newY;
  success();
}

void testProjector4();
void testProjector4() {
  printf("Testing Projector 4:\n");
  size_t BASE_len = 10;
  uchar BASE[10] = {'A', 'C', 'T', 'A', 'C', 'T', 'A', 'A' , 'G', 'G'};  // NOLINT

  for (size_t times = 0; times <= 10; times++) {
    uchar *X;
    uchar *Y;
    size_t XY_len;
    

    uchar *A;
    uchar *B;
    size_t AB_len;

    Utils::GenerateAlignment(BASE, BASE_len, &B, &A, &AB_len);
    Utils::GenerateAlignment(BASE, BASE_len, &X, &Y, &XY_len);
    /*
    printf("*****\n");
    Debug::PrintArray(A, AB_len);
    Debug::PrintArray(B, AB_len);
    Debug::PrintArray(X, XY_len);
    Debug::PrintArray(Y, XY_len);
    printf("*****\n");
    */
    uchar * newA;
    uchar * newY;
    size_t new_len;

    Utils::ProjectAlignment(A, B, AB_len, X, Y, XY_len, &newA, &newY, &new_len);


    delete [] X;
    delete [] Y;
    delete [] A;
    delete [] B;
    delete [] newA;
    delete [] newY;
  }
  success();
}




int main() {
  testProjector1();
  testProjector2();
  testProjector3();
  testProjector4();
  if (!global_fail) {
    printf(ANSI_COLOR_GREEN "\n\tALL UNIT TEST PASSED\n" ANSI_COLOR_RESET);
  } else {
    printf(ANSI_COLOR_RED "\n\tSOME UNIT TESTS FAILED\n" ANSI_COLOR_RESET);
  }
}
