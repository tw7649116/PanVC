/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <sys/times.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <vector>
#include "./utils.h"
#include "./debug.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

  
char * output_prefix;

void printUssage();
void printUssage() {
  printf("Ussage:\n\n");
  printf("./projector A1.plain A2.plain B1.plain B2.plain output_prefix\n"); 
}

void SaveAlignment(uchar * seq1, uchar* seq2, size_t seq_len);
void SaveAlignment(uchar * seq1, uchar* seq2, size_t seq_len) {
  char * file_name = new char[1024];
  sprintf(file_name, 
          "%s.msa", 
          output_prefix);
  FILE * file_seq;
  file_seq = fopen(file_name, "w");
  if (file_seq == NULL) {
    Utils::AbortPrint("SaveChar: could not open file for: %s \n", file_name);
  }
  
  // TODO: retain reference name from the original file...
  const char * head_1 = ">Reference"; 
  const char * head_2 = ">Donor"; 
  
  if (fwrite(head_1, sizeof(uchar), 10, file_seq) != (10)) 
    Utils::AbortPrint("Error in SaveChar, write 1st header\n)");
  if (fwrite("\n", sizeof(uchar), 1, file_seq) != (1)) 
    Utils::AbortPrint("Error in SaveChar, newline\n)");

  if (fwrite(seq1, sizeof(uchar), seq_len, file_seq) != (seq_len)) 
    Utils::AbortPrint("Error in SaveChar, write 1st line\n)");
  if (fwrite("\n", sizeof(uchar), 1, file_seq) != (1)) 
    Utils::AbortPrint("Error in SaveChar, newline\n)");

  if (fwrite(head_2, sizeof(uchar), 6, file_seq) != (6)) 
    Utils::AbortPrint("Error in SaveChar, write 2nd header\n)");
  if (fwrite("\n", sizeof(uchar), 1, file_seq) != (1)) 
    Utils::AbortPrint("Error in SaveChar, newline\n)");

  if (fwrite(seq2, sizeof(uchar), seq_len, file_seq) != (seq_len)) 
    Utils::AbortPrint("Error in SaveChar, write 1st line\n)");
  if (fwrite("\n", sizeof(uchar), 1, file_seq) != (1)) 
    Utils::AbortPrint("Error in SaveChar, newline\n)");

  printf("Adhoc ref saved as: %s \n", file_name);
  fclose(file_seq);
  delete[] file_name;
}

int main(int argc, char ** argv) {
  if (argc != 6) {
    printUssage();
    exit(-1);
  }
  uchar *A, *B, *X, *Y;
  size_t A_len, B_len, X_len, Y_len;
  
  A = Utils::LoadUcharFile(argv[1], &A_len);
  B = Utils::LoadUcharFile(argv[2], &B_len);
  X = Utils::LoadUcharFile(argv[3], &X_len);
  Y = Utils::LoadUcharFile(argv[4], &Y_len);
  output_prefix = argv[5];
  printf("flag 0\n");
  fflush(stdout);

  uchar * newA, *newY;
  size_t new_len;
  Utils::ProjectAlignment(A, B, A_len, X, Y, X_len, &newA, &newY, &new_len);
  SaveAlignment(newA, newY, new_len); 
  printf("Projector finished successfully\n");
}
