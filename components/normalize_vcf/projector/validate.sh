#!/bin/bash
function pause(){
read -n1 -r -p "$*"
}

make clean; make;

TOOL='valgrind --db-attach=yes --leak-check=full --show-reachable=yes'
#TOOL="gdb --args"
BIN_FILE='./unit_tests'

chmod 755 $BIN_FILE

$TOOL $BIN_FILE

