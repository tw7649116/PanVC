/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <sys/times.h>
#include <stdarg.h>
#include <unistd.h>
#include <inttypes.h>
#include <ctime>
#include <climits>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <set>
#include <cstring>
#include <vector>
#include "./utils.h"
#include "./debug.h"

using std::vector;

uchar gen_alph[4] = {'A', 'C', 'G', 'T'};

/* Time meassuring */
double ticks = (double)sysconf(_SC_CLK_TCK);
struct tms t1, t2;
void Utils::StartClock() {
  times(&t1);
}

double Utils::StopClock() {
  times(&t2);
  return (t2.tms_utime-t1.tms_utime)/ticks;
}

bool Utils::Coin() {
 return rand()%2;
}

uchar Utils::RandomChar() {
  return gen_alph[rand()%4];
}

bool Utils::SameUnderlyingSequence(uchar * A, size_t A_len, 
                                   uchar * B, size_t B_len) {
  
  size_t i = 0;
  size_t j = 0;
  while(i < A_len && j < B_len) {
    while(i < A_len && A[i] == '-') i++;
    while(j < B_len && B[j] == '-') j++;
    if (i >= A_len || j >= B_len) {
      break;
    } else {
      if (A[i] != B[j]) {
        return false;
      }
      else {
        i++;
        j++;
      }
    } 
  }
  while(i < A_len && A[i] == '-') i++;
  while(j < B_len && B[j] == '-') j++;
  if (i != A_len || j != B_len) 
    return false;
  else
    return true;
}

void Utils::ProjectAlignment(uchar * A,
                             uchar * B,
                             size_t A_len,
                             uchar * X,
                             uchar * Y,
                             size_t X_len,
                             uchar ** ansA,
                             uchar ** ansY,
                             size_t * new_len) {
  if (! SameUnderlyingSequence(B, A_len, X, X_len)) {
    AbortPrint("Cannot project the alignment, B and X have different underlying sequences");
  }

  std::vector<uchar> newA;
  std::vector<uchar> newY;
  
  newA.reserve(std::max(X_len, A_len));
  newY.reserve(std::max(X_len, A_len));

  size_t i = 0;
  size_t j = 0;
  while (i < A_len || j < X_len) {
    uchar a, b, x, y;
    if (i < A_len) {
      a = A[i]; 
      b = B[i];
    } else {
      a = '-'; 
      b = '-';
    }
    
    if (j < X_len) {
      x = X[j]; 
      y = Y[j]; 
    } else {
      x = '-'; 
      y = '-';
    } 
    assert(b == x || b == '-' || x == '-');

    if (b == x) {
      /*
      if (b == '-') {
        // both are gap:
        i++;
        j++;
        continue;
      }
      */
      newA.push_back(a);
      newY.push_back(y);
      i++;
      j++;
    } else if (b =='-') {
      newA.push_back(a);
      newY.push_back('-');
      i++;
    } else if (x == '-') {
      newA.push_back('-');
      newY.push_back(y);
      j++;
    } else {
      AbortPrint("Impossible happened\n");
    }
  }
 
   
  assert(newA.size() == newY.size());
  *new_len = newA.size();

  uchar * tmpA = new uchar[(*new_len)];
  uchar * tmpY = new uchar[(*new_len)];

  for (i = 0; i < (*new_len); i++) {
    tmpA[i] = newA[i];
    tmpY[i] = newY[i];
  }
  
  /*
  printf("*****\n");
  Debug::PrintArray(tmpA, *new_len);
  Debug::PrintArray(tmpY, *new_len);
  printf("*****\n");
  */

  if (! SameUnderlyingSequence(tmpA, (*new_len), A, A_len)) {
    if (*new_len < 100) {
      Debug::PrintArray(tmpA, *new_len);
      Debug::PrintArray(A, A_len);
    }
    AbortPrint("[projector.cpp] I failed. underlying seqs differ among newA and A");
  }
  if (! SameUnderlyingSequence(tmpY, (*new_len), Y, X_len)) {
    if (*new_len < 100) {
      Debug::PrintArray(tmpY, *new_len);
      Debug::PrintArray(Y, X_len);
    }
    AbortPrint("[projector.cpp] I failed. underlying seqs differ among newY and Y");
  }
  
  *ansA = tmpA;
  *ansY = tmpY;
}

void Utils::GenerateAlignment(uchar* BASE, size_t BASE_len, uchar **A, uchar **B, size_t * ans_len) {
  size_t new_len = 2*BASE_len;

  *ans_len = new_len;
  (*A) = new uchar[new_len];
  (*B) = new uchar[new_len];
  
  for (size_t i = 0; i < new_len; i++) {
    (*A)[i] = '-';
    (*B)[i] = '-';
  }
  size_t offset = 0;
  size_t max_offset = new_len - BASE_len;
  for (size_t i = 0; i < BASE_len; i++) {
    if (offset < max_offset) {
      if (Utils::Coin()) {
        offset+= (size_t)rand()%(max_offset - offset);
        assert(offset < max_offset);
      }   
    }
    (*A)[i+offset] = BASE[i];
    (*B)[i+offset] = BASE[i];
  }
  
  for (size_t i = 0; i < new_len; i++) {
    (*B)[i] = Utils::RandomChar();
  }

}

uchar * Utils::LoadUcharFile(char * file_name, size_t * ans_len) {
  FILE *fp;
  size_t length;
  uchar *buffer;

  fp = fopen(file_name, "r");
  if (!fp) Utils::AbortPrint("Error in LoadCharFile(%s): Cannot open file.", file_name);

  fseeko(fp, 0L, SEEK_END);
  off_t len = ftello(fp);
  if (len == 0) {
    Utils::AbortPrint("Error in LoadCharFile(%s): len = 0\n)", file_name);
  }
  length = (size_t)len;
  rewind(fp);

  buffer = new uchar[length];
  if (!buffer) Utils::AbortPrint("Error in LoadCharFile, new failed. \n)");

  /* copy the file into the buffer */
  if (length !=fread(buffer, sizeof(uchar), length, fp)) {
    Utils::AbortPrint("Error in LoadCharFile: fread failed \n)");
  }
  // printf("We read %i bytes\n", (int)length);
  fclose(fp);
  if (buffer[length-1] == '\n' || buffer[length-1]=='\r' || buffer[length-1]== '\0' ) 
    length--;
  *ans_len = length;
  return buffer;
}

void Utils::AbortPrint(const char* format, ...) {
  va_list args;
  char * buffer = new char[1024];
  va_start(args, format);
  vsprintf(buffer, format, args);
  va_end(args);

  fprintf(stderr, "\n***********\nABORTING::  %s ::\n***********\n", buffer);
  fflush(stderr);
  delete[]buffer;
  exit(-1);
}

// naivest pattern matching
vector<size_t> Utils::NaiveFind(uchar * text,
                                size_t text_length,
                                uchar * pattern,
                                size_t pattern_length) {
  vector<size_t> ans;
  for (size_t i = 0; i <= text_length - pattern_length; i++) {
    size_t j = 0;
    for (; j < pattern_length; j++) {
      if (text[i + j] != pattern[j] )
        break;
    }
    if (j == pattern_length)
      ans.push_back(i);
  }
  return ans;
}

int ucharToInt(uchar c);
int ucharToInt(uchar c) {
  return static_cast<int>(c);
}

void Utils::preBmBc(uchar *pattern, size_t m, size_t bmBc[], size_t ASIZE) {
  size_t i;
  for (i = 0; i < ASIZE; ++i)
    bmBc[i] = m;
  for (i = 0; i < m - 1; ++i)
    bmBc[ucharToInt(pattern[i])] = m - i - 1;
}

vector<size_t>  Utils::Horspool(uchar *text, size_t n, uchar *pattern, size_t m) {
  size_t ASIZE = 0;
  for (size_t i = 0; i < n; i++) {
    if (size_t(text[i]) > ASIZE)
      ASIZE = (size_t)text[i];
  }
  ASIZE++;

  size_t * bmBc;
  bmBc = new size_t[ASIZE];
  preBmBc(pattern, m, bmBc, ASIZE);
  vector<size_t> ans;

  Horspool(text, 0, n-1, pattern, m, bmBc, &ans);
  delete [] bmBc;
  return ans;
}
