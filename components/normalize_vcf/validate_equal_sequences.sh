#!/usr/bin/env bash
set -o errexit
set -o nounset

if [ "$#" -ne 2 ]; then
  echo "Script: '${0}':"
  echo "Illegal number of parameters. I use 2 parameters instead of $# " 
  exit 33
else
  echo "OK Params"
fi

SEQ_1=${1}
SEQ_2=${2}


if [ ! -f $1 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${1}'. Quitting. "
  exit 33
fi

if [ ! -f $2 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${1}'. Quitting. "
  exit 33
fi

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

CLEAN_1=${DIR}/.tmp_1.clean
CLEAN_2=${DIR}/.tmp_2.clean

#cat ${SEQ_1} | tr -d '-' | tr -d '\n' > ${CLEAN_1}
#cat ${SEQ_2} | tr -d '-' | tr -d '\n' > ${CLEAN_2}
cat ${SEQ_1} | tr -d '-'  > ${CLEAN_1}
cat ${SEQ_2} | tr -d '-'  > ${CLEAN_2}


if cmp -s "${CLEAN_1}" "${CLEAN_2}"
then
  echo "The files match, I'm happy"
else
  echo "The files are different, returning an error. Inputs: "
  echo "${1}"
  echo "${2}"
  echo "Underlying seqs: "
  echo "${CLEAN_1}"
  echo "${CLEAN_2}"
  exit 33;
fi

