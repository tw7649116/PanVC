#!/usr/bin/env bash
set -o errexit
set -o nounset

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

PATHS_BIN=${DIR}/src/index_coverage_matrix
#
#  This is the actual script called from the pipeline to compute the heaviest path and the
#  "Ad-hoc reference"
#


if [ "$#" -ne 4 ]; then
  echo "Illegal number of parameters. I should be called from the pipeline script, with 9 params instead of $# " 
  exit 33
fi
  echo "OK Params"

PAN_REFERENCE_FILE=${1}
POS_FOLDER=${2}
MAX_RECOMBINATIONS=${3}
OUTPUT_FOLDER=${4}
if [ ! -f $1 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${1}'. Quitting. "
  exit 33
fi
if [ ! -d $2 ]; then
  echo "Script: '${0}'"
  echo "Failed to find FOLDER '${2}'. Quitting. "
  exit 33
fi
if [ ! -d $4 ]; then
  echo "Script: '${0}'"
  echo "Failed to find FOLDER '${2}'. Quitting. "
  exit 33
fi

#TODO change names Ref -> tmp, except last.
REF_FOLDER=$(dirname "${PAN_REFERENCE_FILE}")
REF_BASE=`basename $PAN_REFERENCE_FILE`
REF_PREFIX=${REF_BASE%.*}
PAN_REFERENCE_FOLDER=${REF_FOLDER}/${REF_PREFIX}.index/

N_REFS=$( zcat ${PAN_REFERENCE_FILE}  | tail -n1 | wc -c)
N_REFS=`expr $N_REFS - 1`
echo "Reference contains ${N_REFS} individuals"

FULL_SEQ=${PAN_REFERENCE_FOLDER}/recombinant.n1.full
LENGTH=$(cat $FULL_SEQ | wc -m)

echo "MSA Length: $LENGTH"

SKIP_ZEROS=0

OUTPUT_PREFIX="${OUTPUT_FOLDER}/adhoc_reference"
PARAMS_1="${N_REFS} ${LENGTH} ${PAN_REFERENCE_FOLDER} ${POS_FOLDER} ${MAX_RECOMBINATIONS} ${SKIP_ZEROS} ${OUTPUT_PREFIX}"

echo "[Command: ${PATHS_BIN} ${PARAMS_1} ]"
${PATHS_BIN} ${PARAMS_1} 

OUTPUT_FULL="${OUTPUT_FOLDER}/adhoc_reference.aligned_to_ref"
OUTPUT_PLAIN="${OUTPUT_FOLDER}/adhoc_reference.plain"
OUTPUT_FASTA="${OUTPUT_FOLDER}/adhoc_reference.fasta"

cat ${OUTPUT_FULL} | tr -d '-' > ${OUTPUT_PLAIN}

echo ">adhoc_ref" > ${OUTPUT_FASTA}
cat ${OUTPUT_PLAIN}  >> ${OUTPUT_FASTA}

echo "Adhoc-ref succesfully built!"
