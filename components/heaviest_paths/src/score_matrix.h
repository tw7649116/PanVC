/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SCORE_MATRIX_H
#define SCORE_MATRIX_H

#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include "./basic.h"
#include "./plain_cube.h"


class ScoreMatrix {
  protected:
    size_t n_seqs;
    size_t length;
    score_t * scores;
  public:
    ScoreMatrix();
    ScoreMatrix(size_t n_sequences, size_t length);
    void Load(char * file_name);
    void Save(char * file_name);
    score_t Get(size_t i, size_t j) {
      return scores[i * length + j];
    }
    void Set(size_t i, size_t j, score_t val) {
      scores[i * length + j] = val;
    }
    size_t GetLength() {
      return length;
    }
    size_t GetNSeqs() {
      return n_seqs;
    }
    void IncrementScore(size_t sequence, size_t pos, size_t length);
    void Print();
    void PrintLevel(PlainCube * matrix, size_t level);
    ~ScoreMatrix();
};
#endif
