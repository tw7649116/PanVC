/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_UTILS_H
#define SRC_UTILS_H
#include <SequenceBuilder.h>

class Utils {
  public:
    static void StartClock();
    static double StopClock();
    static uchar * LoadCharFile(char * file_name, size_t * ans_len);
    static void AbortPrint(const char* format, ...);
};
#endif
