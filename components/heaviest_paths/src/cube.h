/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_CUBE_H_ 
#define SRC_CUBE_H_ 

#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include "./basic.h"

class Cube {
 protected:
  size_t n_seqs;
  size_t length;
  size_t depth;
  score_t max_score;
 public:
  size_t GetLength() {
    return length;
  }
  size_t GetNSeqs() {
    return n_seqs;
  }
  virtual size_t SpaceRequirementBits() = 0;
  virtual void Set(size_t x, size_t y, size_t z, score_t val) = 0;
  virtual score_t Get(size_t x, size_t y, size_t z) = 0;
  
  virtual void TwoMax(size_t j,
                      size_t l,
                      size_t * max_seq,
                      size_t * second_max_seq,
                      score_t * max_val,
                      score_t * second_max_val);
  virtual void Max(size_t j, size_t l, size_t * max_seq, score_t * max_val);
};

#endif
