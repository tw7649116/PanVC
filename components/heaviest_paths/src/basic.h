/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#define __STDC_FORMAT_MACROS
// for printing PRIu64
#include <inttypes.h>
#include <stdint.h>
#include <limits.h>
#ifndef BASIC_H
#define BASIC_H

typedef unsigned int uint;
typedef unsigned char uchar;
typedef int score_t;

inline uint bits(uint number) {
  uint counter = 0;
  while (number) {
    number >>= 1;
    counter++;
  }
  return counter;
}

#endif  // BASIC_H
