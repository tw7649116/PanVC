/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_SUCCINCT_CUBE_H_ 
#define SRC_SUCCINCT_CUBE_H_

#include <libcdsBitString.h>
#include <BitSequence.h>
#include <BitSequenceRG.h>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include "./basic.h"
#include "./cube.h"

//class SuccinctCube {
class SuccinctCube : public Cube {
 protected:
  score_t default_score;
  size_t current_y;
  score_t * current_layer_data;
  score_t * previous_layer_data;
  BitString ** bitmaps_data;
  cds_static::BitSequenceRG ** bitmaps_dictionary;
  bool is_modifiable;
  void UpdateLayers();
  void UpdateBitmaps(size_t layer_y);
 public:
  SuccinctCube(size_t x, size_t y, size_t z, score_t max_score, score_t init);
  // assumes (y == current_y || y == current_y + 1)
  void Set(size_t x, size_t y, size_t z, score_t val);
  score_t Get(size_t x, size_t y, size_t z);
  size_t SpaceRequirementBits();
  void PrepareForQueries();
  ~SuccinctCube();
};

#endif
