/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_PARTIAL_CUBE_H_ 
#define SRC_PARTIAL_CUBE_H_ 

#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include "./basic.h"
#include "./cube.h"

class PartialCube : public Cube {
 protected:
  std::vector<score_t> m_data;
  size_t first_persistent_layer;
  size_t mini_cube_layers;
  size_t current_y;
  score_t default_score;
  score_t * current_layer_data;
  score_t * previous_layer_data;
  void UpdateLayers();
 public:
  PartialCube(size_t x,
              size_t y,
              size_t z,
              size_t n_layers,
              score_t _max_score,
              score_t init);
  
  size_t SpaceRequirementBits();
  void Set(size_t x, size_t y, size_t z, score_t val);
  score_t Get(size_t x, size_t y, size_t z);
  ~PartialCube();
};

#endif
