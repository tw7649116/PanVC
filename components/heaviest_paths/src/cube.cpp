/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include "./cube.h"
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <vector>
#include <algorithm>
#include "./basic.h"
#include "./debug.h"

void Cube::TwoMax(size_t j,
                     size_t l,
                     size_t * max_seq,
                     size_t * second_max_seq,
                     score_t * max_val,
                     score_t * second_max_val) {
  assert(n_seqs >= 2);
  size_t m_seq = 0;
  score_t m_val = this->Get(0, j, l);

  size_t s_seq = 1;
  score_t s_val = this->Get(1, j, l);

  if (s_val > m_val) {
    score_t t_val = m_val;
    size_t t_seq = m_seq;

    m_val = s_val;
    m_seq = s_seq;

    s_val = t_val;
    s_seq = t_seq;
  }
  for (size_t s = 2; s < n_seqs; s++) {
    score_t curr_val = this->Get(s, j, l);
    if (curr_val > s_val) {
      if (curr_val > m_val) {
        // replace m:
        s_val = m_val;
        s_seq = m_seq;

        m_val = curr_val;
        m_seq = s;
      } else {
        // replace s:
        s_val = curr_val;
        s_seq = s;
      }
    }
  }
  *max_val = m_val;
  *max_seq = m_seq;
  *second_max_seq = s_seq;
  *second_max_val = s_val;
  return;
}

void Cube::Max(size_t j, size_t l, size_t * max_seq, score_t * max_val) {
  size_t m_seq = 0;
  score_t m_val = this->Get(0, j, l);
  for (size_t seq = 1; seq < n_seqs; seq++) {
    if (this->Get(seq, j, l) > m_val) {
      m_val = this->Get(seq, j, l);
      m_seq = seq;
    }
  }
  *max_seq = m_seq;
  *max_val = m_val;
  return;
}
