#!/usr/bin/env bash
set -o errexit
set -o nounset

function pause(){
read -n1 -r -p "$*"
}

# Test with random matrix.
# It ilustrates how to explore the performance of our algorithms for different sizes.

SCORE_FILE_NAME='tmp_scores.tmp'
N_SEQS='5'
LENGTH='64000'
#MIN_CHANGES='1'
#MAX_CHANGES='3'
#CHANGES_STEP='1'
MIN_CHANGES='20'
MAX_CHANGES='20'
CHANGES_STEP='5'

FLAGS='--db-attach=yes --leak-check=full --show-reachable=yes'
TOOL="valgrind $FLAGS"

BIN_FILE_0='./unit_tests'

BIN_FILE_1='./random_matrix'
PARAMS_1="$N_SEQS $LENGTH $SCORE_FILE_NAME"

BIN_FILE_2='./measure_matrix'
PARAMS_2="$SCORE_FILE_NAME $MIN_CHANGES $MAX_CHANGES $CHANGES_STEP"

make clean;
make;
chmod 755 $BIN_FILE_0
chmod 755 $BIN_FILE_1
chmod 755 $BIN_FILE_2


$TOOL $BIN_FILE_0

printf "\n"
pause 'Unit tests done.'
printf "\n"

$TOOL $BIN_FILE_1 $PARAMS_1

printf "\n"
pause 'Score matrix with random values built. Press any key to evaluate the algors.'
printf "\n"

$TOOL $BIN_FILE_2 $PARAMS_2

pause 'Tests finnished'

