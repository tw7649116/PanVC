/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <sys/times.h>
#include <stdarg.h>
#include <unistd.h>
#include <inttypes.h>
#include <ctime>
#include <climits>
#include <cassert>
#include <set>
#include "./utils.h"
#include "./basic.h"

/* Time meassuring */
double ticks = (double)sysconf(_SC_CLK_TCK);
struct tms t1, t2;
void Utils::StartClock() {
  times(&t1);
}

double Utils::StopClock() {
  times(&t2);
  return (t2.tms_utime-t1.tms_utime)/ticks;
}

uchar * Utils::LoadCharFile(char * file_name, size_t * ans_len) {
  FILE *fp;
  size_t length;
  uchar *buffer;

  fp = fopen(file_name, "r");
  if (!fp) Utils::AbortPrint("Error in LoadCharFile A\n)");

  fseeko(fp, 0L, SEEK_END);
  off_t len = ftello(fp);
  if (len == 0) Utils::AbortPrint("Error in LoadCharFile(%s): len = 0\n)", file_name);
  length = (size_t)len;
  rewind(fp);

  buffer = new uchar[length];
  if (!buffer) Utils::AbortPrint("Error in LoadCharFile B\n)");

  /* copy the file into the buffer */
  if (length !=fread(buffer, sizeof(uchar), length, fp)) {
    Utils::AbortPrint("Error in LoadCharFileC \n)");
  }
  // printf("We read %i bytes\n", (int)length);
  fclose(fp);
  *ans_len = length;
  return buffer;
}

void Utils::AbortPrint(const char* format, ...) {

  va_list args;
  char * buffer = new char[1024];
  va_start(args, format);
  vsprintf(buffer, format, args);
  va_end(args);

  fprintf(stderr, "\n***********\nABORTING::  %s ::\n***********\n", buffer);
  fflush(stderr);
  delete[]buffer;
  exit(-1);
}
