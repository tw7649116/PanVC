/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
#include <cassert>
#include "./basic.h"
#include "./debug.h"
#include "./score_matrix.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

void printUssage();
void printUssage() {
  printf("Ussage:\n\n");
  printf("./random_matrix n_sequences length output_file_name\n");
}

int main(int argc, char ** argv) {
  if (argc != 4) {
    printUssage();
    exit(-1);
  }

  size_t n_sequences = (size_t)atoi(argv[1]);
  size_t length = (size_t)atoi(argv[2]);
  char * output_file_name = argv[3];


  ScoreMatrix * scores_matrix = new ScoreMatrix(n_sequences, length);
  int times = 20;
  for (size_t seq = 0; seq < n_sequences; seq++) {
    for (int i = 0; i < times; i++) {
      size_t pos = (size_t)rand() % (length/2);
      size_t read_length = 1 + (size_t)rand() % (length/2 -2);
      scores_matrix->IncrementScore(seq, pos, read_length);
    }
  }

  scores_matrix->Save(output_file_name);

  delete(scores_matrix);
}


