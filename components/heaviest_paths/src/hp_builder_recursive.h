/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#ifndef SRC_RECURSIVE_HEAVIEST_PATH_H_
#define SRC_RECURSIVE_HEAVIEST_PATH_H_ 
#include <SequenceBuilder.h>
#include "./succinct_cube.h"
#include "./plain_cube.h"
#include "./score_matrix.h"
#include "./hp_builder.h"

class RecursiveHPBuilder : public HeaviestPath {
 private:
  size_t max_allowed_bits;
 public:
  RecursiveHPBuilder(size_t max_bits); 
  size_t * RestrictedHeaviestPath(ScoreMatrix * scores,
                                  size_t changes_allowed, 
                                  score_t max_score, 
                                  score_t  * total_weight,
                                  size_t * bits_required);
  void SetMaxAllowedBits(size_t max_bits);
};
#endif
