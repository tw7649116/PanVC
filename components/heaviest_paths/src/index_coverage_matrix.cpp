/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi

 Load files with the info of mapped reads into each sequence (ie: rows),
 build the matrix, and compute the optimal path, for the given constrains.

 */

#include <sys/times.h>
#include <BitSequence.h>
#include <BitSequenceRG.h>
#include <libcdsBitString.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <cstdlib>
#include <cassert>
#include "./hp_builder_plain.h"
#include "./utils.h"
#include "./basic.h"
#include "./debug.h"
#include "./score_matrix.h"
#include "./hp_builder.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

size_t msa_length;  
char * pos_folder;
char * sequences_folder;
char * output_prefix;

void printUssage();
void printUssage() {
  printf("Ussage:\n\n");
  printf("./index_coverage_matrix n_refs msa_length  sequences_folder pos_folder max_recombinations do_skip_zeros output_prefix\n"); 
}

char * getFileNameMatches(int sequence_id);
char * getFileNameMatches(int sequence_id) {
  char * ans = new char[1024];
  sprintf(ans, "%s/mapped_reads_to%i.pos", pos_folder, sequence_id);
  return ans;
}

char * getFileNameGaps(int sequence_id);
char * getFileNameGaps(int sequence_id) {
  char * ans = new char[1024];
  sprintf(ans, "%s/recombinant.n%i.gap_positions", sequences_folder, sequence_id);
  return ans;
}


cds_static::BitSequence * getGapMapper(size_t strain_id);
cds_static::BitSequence * getGapMapper(size_t strain_id) {
  BitString bitmap_data(msa_length);

  char * file_name = getFileNameGaps(strain_id);
  std::ifstream fs(file_name);
  if(!fs.good()) {
    std::cout << "problem reading " << file_name << std::endl ;
    exit(-1); 
  }
  std::string line;
  int i = 0;
  while (std::getline(fs, line)) {
    stringstream ss(line);
    size_t pos;
    if(!(ss >> pos)) {
      std::cout << "Bad Line reading " << file_name << std::endl ;
      exit(-1); 
    }
    assert(pos < msa_length);
    bitmap_data.setBit(pos, true);

    i++;
  }
  // std::cout << i << "gaps read" << std::endl;
  cds_static::BitSequenceRG* bsRG = new cds_static::BitSequenceRG(bitmap_data, 20);
  delete[] file_name;
  return bsRG;
} 

// increment the positions corresponding in the matrix.
// this implie, because of gaps in the MSA that the matrix models,
// that the real length of the match might be bigger.
void IncrementRowFromPatterns(ScoreMatrix * scores,
                              std::ifstream *fs,
                              cds_static::BitSequence* mapper,
                              size_t row_id);
void IncrementRowFromPatterns(ScoreMatrix * scores,
                              std::ifstream *fs,
                              cds_static::BitSequence* mapper,
                              size_t row_id) {

  printf("reading a positions file\n");
  std::string line;
  int i = 0;
  while (std::getline(*fs, line)) {
    // std::cout << "Reading interval:" <<std::endl;
    // std::cout << line << std::endl;
    stringstream ss(line);
    size_t pos;
    if(!(ss >> pos)) {
      std::cout << "Bad Line while reading. " << std::endl ;
      exit(-1); 
    }
    size_t real_pos = mapper->select0(1+pos);
    size_t match_length;
    if(!(ss >> match_length)) {
      std::cout << "Bad Line while reading. " << std::endl ;
      exit(-1); 
    }
    size_t end_pos = mapper->select0(pos+match_length);
    size_t real_length = end_pos- real_pos + 1 ;
    scores->IncrementScore(row_id, real_pos, real_length);
    i++;
  }
  //std::cout << i << "lines\n";
  /*
   */
}

uchar * LoadStrainFull(size_t strain_id);
uchar * LoadStrainFull(size_t strain_id) {
  char * file_name = new char[1024];
  sprintf(file_name, "%s/recombinant.n%i.full", sequences_folder, (int)strain_id);
  size_t tmp_len;
  uchar * ans =  Utils::LoadCharFile(file_name, &tmp_len);
  delete [] file_name;
  return ans;
}


void SavePredicted(uchar * seq);
void SavePredicted(uchar * seq) {
  char * file_name = new char[1024];
  sprintf(file_name, 
          "%s.aligned_to_ref", 
          output_prefix);
  FILE * file_seq;
  file_seq = fopen(file_name, "w");
  if (file_seq == NULL) {
    Utils::AbortPrint("SaveChar: could not open file for: %s \n", file_name);
  }
  if (fwrite(seq, sizeof(uchar), msa_length, file_seq) != (msa_length)) 
    Utils::AbortPrint("Error in SaveChar, write\n)");
  printf("Adhoc ref saved as: %s \n", file_name);
  fclose(file_seq);
  delete[] file_name;
}

uchar * ExtractPredicted(size_t * path,
                         size_t first_ref,
                         size_t last_ref,
                         ScoreMatrix * scores,
                         bool skip_zeros);
uchar * ExtractPredicted(size_t * path,
                         size_t first_ref,
                         size_t last_ref,
                         ScoreMatrix * scores,
                         bool skip_zeros) {
  uchar * answer = new uchar[msa_length];
  for (size_t strain_id = first_ref; strain_id <= last_ref; strain_id++) {
    uchar * current_strain = LoadStrainFull(strain_id);
    for (size_t i = 0; i < msa_length; i++) {
      if (path[i] == (strain_id - first_ref)) { 
        answer[i] = current_strain[i];
      }
    }
    delete[] current_strain;
  }
  // Replace with - positions that weigth zero:
  size_t count_zeros = 0;
  for (size_t i = 0; i < msa_length; i++) {
    if (scores->Get(path[i], i) == 0) { 
      if (skip_zeros) {
        answer[i] = '-';
      }
      count_zeros++;
      //printf("ZW - in pos %lu\n",i);
    }
  }
  printf("%lu Zero-Weight cells found.\n",count_zeros);
  if (skip_zeros) {
    printf("Replaced with gaps.\n");
  } else {
    printf("Untouched.\n");
  } 
  return answer;
}
int main(int argc, char ** argv) {
  if (argc != 8) {
    printUssage();
    exit(-1);
  }

  size_t first_ref = 1;
  size_t last_ref = (size_t)atoi(argv[1]);

  msa_length = (size_t)atoi(argv[2]);
  sequences_folder = argv[3];
  pos_folder = argv[4];

  size_t max_recombinations = (size_t)atoi(argv[5]);

  assert(atoi(argv[6]) == 0 || atoi(argv[6]) == 1);
  bool skip_zeros = atoi(argv[6]);
  output_prefix = argv[7];

  // TODO: move to a different file "build matrix"
  // building of the score matrix from the previous files:
  size_t n_seqs = last_ref - first_ref + 1;
  ScoreMatrix * scores = new ScoreMatrix(n_seqs, msa_length);

  size_t row_id = 0;
  for (size_t strain_id = first_ref; strain_id <= last_ref; strain_id++) {
    cds_static::BitSequence* mapper = getGapMapper(strain_id);

    char * file_name = getFileNameMatches(strain_id);
    std::ifstream fs(file_name);
    if(!fs.good()) {
      std::cout << "problem reading " << file_name << std::endl ;
      exit(-1); 
    }
    assert(row_id == strain_id -first_ref);
    IncrementRowFromPatterns(scores, &fs, mapper, row_id);
    delete(mapper);
    delete[] file_name;
    row_id++;
  }


  score_t max_score;
  bool do_print = false;
  if (msa_length <= 210) {
    do_print = true;
  }


  size_t * best_path;
  uchar * predicted_sequence;
  // computation of heaviest path:
  if (max_recombinations == 0) {
    best_path = HeaviestPath::Unrestricted(scores, &max_score);
    std::cout << " Unrestricted score: " << max_score << std::endl;

    predicted_sequence = ExtractPredicted(best_path,
                                          first_ref,
                                          last_ref,
                                          scores,
                                          skip_zeros);
    SavePredicted(predicted_sequence);
    if (do_print) { 
      scores->Print();
      std::cout << " Best Unrestricted path: " << std::endl;
      Debug::printArray(best_path, msa_length);
      std::cout << " Predicted Sequence : " << std::endl;
      Debug::printArray(predicted_sequence, msa_length);
    }

    delete [] predicted_sequence;
    delete[] best_path;
  } else {

    //*******************************************
    //// RESTRICTED PATHS::::
    PlainHPBuilder * builder = new PlainHPBuilder();
    score_t curr_weight;
    size_t curr_bytes;
    best_path = builder->RestrictedHeaviestPath(scores,
                                                max_recombinations,
                                                max_score,
                                                &curr_weight,
                                                &curr_bytes);
    std::cout << " Restricted " << max_recombinations << " score: " << curr_weight<< std::endl;
    predicted_sequence = ExtractPredicted(best_path,
                                          first_ref,
                                          last_ref,
                                          scores,
                                          skip_zeros);
    SavePredicted(predicted_sequence);

    delete [] predicted_sequence;
    delete [] best_path;
    delete(builder);
  }
  printf("ByeWorld\n");
  delete(scores);
}
