/* Copyright (C) 2013, Daniel Valenzuela, all rights reserved.
 * dvalenzu@cs.helsinki.fi
 */

#include <algorithm>
#include "./hp_builder_plain.h"
#include "./succinct_cube.h"
#include "./cube.h"
#include "./partial_cube.h"
#include "./plain_cube.h"
#include "./score_matrix.h"
#include "./basic.h"

PlainHPBuilder::PlainHPBuilder() {
}

size_t * PlainHPBuilder::RestrictedHeaviestPath(ScoreMatrix * scores,
                                                size_t changes_allowed,
                                                score_t max_score,
                                                score_t  * total_weight,
                                                size_t * space_required) {
  size_t length = scores->GetLength();
  size_t n_seqs = scores->GetNSeqs();
  assert(changes_allowed < length);
  score_t default_score = 0;
  PlainCube matrix(n_seqs,
                   length,
                   changes_allowed + 1,
                   max_score,
                   default_score);
  *space_required = matrix.SpaceRequirementBits();
  ComputeCube(scores, &matrix, changes_allowed);
  // PrintLevel(&matrix, 0);
  // PrintLevel(&matrix, 1);
  return GetFullPath(&matrix, changes_allowed, total_weight);
}


