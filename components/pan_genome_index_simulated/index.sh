#!/usr/bin/env bash
set -o errexit
set -o nounset

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source ${DIR}/config.sh

if [ "$#" -ne 1 ]; then
  echo "Illegal number of parameters. I use 1 params instead of $# " 
  exit 33
fi
echo "OK Params"


PAN_REFERENCE_FILE=$1
if [ ! -f $1 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${1}'. Quitting. "
  exit 33
fi


TMP_FOLDER=$(dirname "${PAN_REFERENCE_FILE}")
TMP_BASE=`basename $PAN_REFERENCE_FILE`
TMP_PREFIX=${TMP_BASE%.*}
PAN_REFERENCE_FOLDER=${TMP_FOLDER}/${TMP_PREFIX}.index/

if [ -d "${PAN_REFERENCE_FOLDER}" ]; then
  if [ ${REUSE_INDEX} -eq 1 ]; then
    echo "Folder-Index '${PAN_REFERENCE_FOLDER}' and flag REUSE_INDEX is set.
    we will not index again. File: '${0}'"
    exit 0
  fi
fi
mkdir -p ${PAN_REFERENCE_FOLDER}

if [ ! -f "${BWA_BIN}" ]; then
  echo "File '${BWA_BIN}'not found!. File: '${0}'"
  exit 33
fi

N_REFS=$( zcat ${PAN_REFERENCE_FILE}  | tail -n1 | wc -c)
N_REFS=`expr $N_REFS - 1`
echo "Reference contains ${N_REFS} individuals"


for (( CURRENT_REFERENCE=1; CURRENT_REFERENCE<=${N_REFS}; CURRENT_REFERENCE++ ))
do
  # Make all index:
  SEQUENCE_FULL=$PAN_REFERENCE_FOLDER/recombinant.n$CURRENT_REFERENCE.full
  SEQUENCE_FILE=$PAN_REFERENCE_FOLDER/recombinant.n$CURRENT_REFERENCE.fa
  echo ">$SEQUENCE_FILE" > $SEQUENCE_FILE
  zcat ${PAN_REFERENCE_FILE} |awk '{printf("%s", substr($0,'${CURRENT_REFERENCE}',1))}' > ${SEQUENCE_FULL}
  cat ${SEQUENCE_FULL} | tr -d '-' >> ${SEQUENCE_FILE}

  GAP_POS_FILE=$PAN_REFERENCE_FOLDER/recombinant.n${CURRENT_REFERENCE}.gap_positions
  python ${GAP_POSITIONS_PY} ${SEQUENCE_FULL} > ${GAP_POS_FILE}

  ## TODO: redirect bwa output to a log file
  $BWA_BIN index $SEQUENCE_FILE #&
done
wait

