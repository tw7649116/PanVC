#!/usr/bin/env bash
set -o errexit
set -o nounset

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source ${DIR}/config.sh

if [ "$#" -ne 3 ]; then
  echo "Illegal number of parameters. I should be called from the pipeline script, with 2 params instead of $# " 
  exit 33
else
  echo "OK Params"
fi

PAN_REFERENCE_FILE=${1}
READS_FILE=${2}
OUTPUT_FOLDER=${3}
if [ ! -f $1 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${1}'. Quitting. "
  exit 33
fi
if [ ! -f $2 ]; then
  echo "Script: '${0}'"
  echo "Failed to find file '${2}'. Quitting. "
  exit 33
fi
# Output folder may not exist, in that case we will create it.

#############################################
#### Computing info from reference...
TMP_FOLDER=$(dirname "${PAN_REFERENCE_FILE}")
TMP_BASE=`basename $PAN_REFERENCE_FILE`
TMP_PREFIX=${TMP_BASE%.*}

PAN_REFERENCE_FOLDER=${TMP_FOLDER}/${TMP_PREFIX}.index/

N_REFS=$( zcat ${PAN_REFERENCE_FILE}  | tail -n1 | wc -c)
N_REFS=`expr $N_REFS - 1`
#############################################


# TODO: check that the DEPENDENCIES exist:
# sam_to_positions.py
# reads_to_fastq.py for the exact reads only
# gap_positions.py

if [ ! -f "${BWA_BIN}" ]; then
  echo "File '${BWA_BIN}'not found!. File: '${0}'"
  exit 33
fi


############################################################# MAIN

## TODO: replace check directory and instead, check every file exists.
## Then, it can be done always.
if [ -d ${OUTPUT_FOLDER} ]; then
  if [ ${REUSE_SAMS} -eq 1 ]; then
    echo "Folder-Sams '${OUTPUT_FOLDER}' and flag REUSE_SAMS is set.
    we will not align the reads again. File: '${0}'"
    exit 0
  fi
  echo "Using an existing output folder to store sam files"
else
  echo "Output folder: ${OUTPUT_FOLDER} does not exists. Creating it ..."
  mkdir -p ${OUTPUT_FOLDER}
fi

echo "Generating SAM FILES"
for (( CURRENT_REFERENCE=1; CURRENT_REFERENCE<=${N_REFS}; CURRENT_REFERENCE++ ))
do
  # Make the calls
  SEQUENCE_FILE=$PAN_REFERENCE_FOLDER/recombinant.n${CURRENT_REFERENCE}.fa
  SAM_FILE=${OUTPUT_FOLDER}/mapped_reads_to${CURRENT_REFERENCE}.sam.gz
  #echo "Command line: $BWA_BIN mem $BWA_FLAGS_THREADS $SEQUENCE_FILE $READS_FILE"
  $BWA_BIN mem $BWA_FLAGS_THREADS $SEQUENCE_FILE $READS_FILE | gzip > $SAM_FILE
done

echo "SAM files generated"
