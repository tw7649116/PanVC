#!/usr/bin/env bash
set -o errexit
set -o nounset

rm -rf tmp_kaka*;
rm -rf tmp_sams;
rm -rf example_data/reference_pg_aligned.index/
rm -rf parametric_test_tmp//

rm -f adhoc_reference.*
